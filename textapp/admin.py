from django.contrib import admin
from .models import *

# Register your models here.
class AppDetailAdmin(admin.ModelAdmin):
    #exclude = ('title',)
    list_filter = ('app',)
    list_display = ("title","app",)
    search_fields = ['title',]
admin.site.register(App)
admin.site.register(AppAdds)
admin.site.register(AppDetail,AppDetailAdmin)
