from django.db import models

class App(models.Model):
    name = models.CharField(max_length=100)

    def __unicode__(self):
        return self.name

class AppAdds(models.Model):
    app = models.ForeignKey(App,related_name='adds')
    image = models.ImageField()
    playURL = models.URLField()

    def __unicode__(self):
        return "image"

class AppDetail(models.Model):
    app = models.ForeignKey(App,related_name='detail')
    title = models.CharField(max_length=150)
    Description = models.TextField(null=True,blank=True)

    def __unicode__(self):
        return self.title
