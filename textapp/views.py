from rest_framework import viewsets
from .models import *
from .serializers import *
from rest_framework import filters

class AppView(viewsets.ModelViewSet):
    queryset = App.objects.all()
    serializer_class = AppSerializer

class AppDetailView(viewsets.ModelViewSet):
    queryset = AppDetail.objects.all().order_by('id')
    serializer_class = AppDetailSerializer
    filter_backends = (filters.DjangoFilterBackend,)
    filter_fields = ('app',)

class AppAddsView(viewsets.ModelViewSet):
    queryset = AppAdds.objects.all()
    serializer_class = AppAddsSerializer
    filter_backends = (filters.DjangoFilterBackend,)
    filter_fields = ('app',)
