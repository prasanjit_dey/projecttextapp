from django.conf.urls import url,include,patterns
import views
from rest_framework.routers import DefaultRouter

router = DefaultRouter()
router.register(r'apps',views.AppView)
router.register(r'appsdetail',views.AppDetailView)
router.register(r'appsadds',views.AppAddsView)

urlpatterns = [
    url(r'^', include(router.urls)),

]
