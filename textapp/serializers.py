from rest_framework import serializers
from .models import *

class AppSerializer(serializers.ModelSerializer):
    class Meta:
        model = App
        fields = ('name',)


class AppDetailSerializer(serializers.ModelSerializer):
    class Meta:
        model = AppDetail
        fields = ('title','Description','app')


class AppAddsSerializer(serializers.ModelSerializer):
    class Meta:
        model = AppAdds
        fields = ('image','app','playURL')