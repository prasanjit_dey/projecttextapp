#!/bin/bash
# Starts the Gunicorn server
set -e

# Activate the virtualenv for this project
source /home/ubuntu/projects/textapp/bin/activate

# Start gunicorn going
exec gunicorn textproject.wsgi:application -c /home/ubuntu/projects/textproject/gunicorn.conf.py 
